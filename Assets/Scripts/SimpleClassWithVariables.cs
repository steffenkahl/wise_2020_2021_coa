﻿using System.Security;

namespace ClassExamples
{
    class SimpleClassWithVariables
    {

        private int     intExampleWithoutValue;
        private float   floatExampleWithoutValue;

        private int     intExampleWithCalue         = 5;
        private float   floatExampleWithValue       = 0.5f;

        private bool    boolExampleWithoutValue;
        private bool    boolExampleWithValue        = true;
        private bool    otherBoolExampleWithValue   = false;

        private string  stringExampleWithoutValue;
        private string  stringExampleWithValue      = "Hallo";
    }
}
