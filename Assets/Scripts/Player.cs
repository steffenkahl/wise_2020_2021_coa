﻿using UnityEngine;

namespace MethodExamples
{
    public class Player : MonoBehaviour
    {
        public int health;

        private void Awake()
        {
            health = 100;    
        }
    }
}
